#include <Servo.h> 

#define SERVO_PIN 11
#define MOISTURE_PIN A0

long water_duration = 2000;
long cycle_duration = 60000; // 1 min
long timer_duration = 600000; // 10 min
int moisture_border = 500;
unsigned long waterMillis = millis();
unsigned long paramsMillis = millis(); 
unsigned long timerMillis = millis(); 

Servo servo1;

const char none_mode = 0;
const char moisture_mode = 1;
const char timer_mode = 2;

char current_mode = none_mode;

const char moisture_key_out = 101;
const char moisture_key_in = 3;
const char timer_duration_key_in = 2;
const char water_duration_key_in = 4;
const char mode_key_in = 1;

struct packet_data {
  char key;
  char value[2];
};

void send_packet(struct packet_data data) { // value in big-endian
  Serial.write(0x7E); // start byte
  
  Serial.write((byte)0x0); // length (2 bytes)
  Serial.write(0x11);
  
  Serial.write(0x10); // Frame type (Transmit Request - 0x10)
  Serial.write((byte)0x1);

  Serial.write((byte)0x0);
  Serial.write((byte)0x0);
  Serial.write((byte)0x0);
  Serial.write((byte)0x0);
  Serial.write((byte)0x0);
  Serial.write((byte)0x0);
  Serial.write(0xFF);
  Serial.write(0xFF);

  Serial.write(0xFF);
  Serial.write(0xFE);

  Serial.write(0x0);
  Serial.write(0x0);

  // payload data
  Serial.write(data.key);
  Serial.write(data.value[0]);
  Serial.write(data.value[1]);


  long sum = 0x10 + 1 + 0xFF + 0xFF + 0xFF + 0xFE + data.key + data.value[0] + data.value[1];
  Serial.write(0xFF - (sum & 0xFF));

}

byte my_Serial_read() {
  byte ret = Serial.read();
//  Serial.print(ret, HEX);
//  Serial.print(" ");
  return ret;
}

struct packet_data read_packet() {
  char values[] = {0, 0};
  struct packet_data data;
  data.key = 0;
  data.value[0] = 0;
  data.value[1] = 0;
  if (Serial.available() >= 4) {
    byte start_byte = 0;
    while (start_byte != 0x7E && Serial.available() > 0) {
      start_byte = my_Serial_read();
    }
    delay(50);
    unsigned short packet_length = ((unsigned short) my_Serial_read()) * 256 + ((unsigned short) my_Serial_read());
    unsigned char packet_type = my_Serial_read();
//    Serial.print("pt = ");
//    Serial.print(packet_type, HEX);
//    Serial.print(" ");
    if (packet_type != 0x90) {
      for (int i = 0; i < packet_length; i++) {
//        Serial.print("D");
        byte discard_byte = my_Serial_read();
      }
    } else {
      for (int i = 0; i < 11; i++) {
//        Serial.print("d");
        byte discard_byte = my_Serial_read();
      }
      data.key = my_Serial_read();
      data.value[0] = my_Serial_read();
      data.value[1] = my_Serial_read();
//      Serial.print("dc");
      byte checksum = my_Serial_read();
    }
  }
  return data;
}

void water(int milliseconds) {
  open_water();
  delay(milliseconds);
  close_water();
}

void open_water() {
  servo1.write(90);
}

void close_water() {
  servo1.write(0);
}

int read_moisture() {
  return analogRead(MOISTURE_PIN);
}

void send_moisture() {
  int moisture = read_moisture();
  struct packet_data data;
  data.key = moisture_key_out;
  data.value[0] = (char) (moisture / 256);
  data.value[1] = (char) (moisture % 256); // big-endian
  send_packet(data);
}

void moisture_mode_cycle() {
  int moisture = read_moisture();
  if (moisture < moisture_border) {
    water(water_duration);
  }
}

void timer_mode_cycle() {
  if (millis() - timerMillis >= timer_duration) {
    timerMillis = millis();
    water(water_duration);
  }
}

void mode_cycle() {
  switch (current_mode) {
    case moisture_mode:
      send_moisture();
      moisture_mode_cycle();
      break;

    case timer_mode:
      send_moisture();
      timer_mode_cycle();
      break;
  }
}

void change_mode(char mode) {
  if (mode != current_mode) {
    current_mode = mode;
  }
}

void params_cycle() {
  struct packet_data data = read_packet();
  if (data.key != 0) {
    switch (data.key) {
      case mode_key_in:
        change_mode(data.value[1]);
        break;
      case moisture_key_in:
        moisture_border = data.value[0] * 256 + data.value[1];
        break;
      case timer_duration_key_in:
        timer_duration = (data.value[0] * 256 + data.value[1]) * 60000;
        break;
      case water_duration_key_in:
        water_duration = (data.value[0] * 256 + data.value[1]) * 1000;
        break;
    }
  }
}

void setup() {
  Serial.begin(9600);
  pinMode(MOISTURE_PIN, INPUT);
  pinMode(SERVO_PIN, OUTPUT);
  servo1.attach(SERVO_PIN);
  close_water();
}

void loop() {
  if (millis() - waterMillis >= cycle_duration) {
    waterMillis = millis();
    mode_cycle();
  }
  params_cycle();
}
